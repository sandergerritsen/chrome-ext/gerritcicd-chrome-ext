# GerritCICD Notifier

Get notified of web application update directly next to the web app itself.

## Chrome Devtools Panel

![Example devtools extension view](doc/image/auto.png "Example devtools extension view")


### Usage

Open the chrome devtools (F12) and navigate to the GerritCICD panel (next to the 'Elements'/'Console' panels).

### Examples
- Manually triggered <a href="example/index.html">example/index.html</a>
- Automatically triggered <a href="example/auto.html">example/auto.html</a>