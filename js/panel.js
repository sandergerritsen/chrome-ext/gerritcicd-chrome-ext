'use strict';
let revert;

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (revert !== 'undefined') {
            clearTimeout(revert);
        }

        const data = JSON.parse(request.content);
        document.body.style.backgroundColor = "green";
        document.getElementById('commitHash').innerText = data.commit.sha;
        document.getElementById('username').innerText = data.commit.username;
        document.getElementById('dateTime').innerText = data.commit.dateTime;
        document.getElementById('message').innerText = data.commit.message;

        revert = setTimeout(function () {
            document.body.style.backgroundColor = "white";
        }, 4000);
    }
);
