'use strict';

const content = function() {
// Select the node that will be observed for mutations
    const targetNode = document.getElementById('contentHash');

    if (targetNode === 'undefined') {
        return;
    }

// Options for the observer (which mutations to observe)
    const config = { characterData: false, attributes: false, childList: true, subtree: false };

// Callback function to execute when mutations are observed
    const callback = function(mutationsList, observer) {
        // Use traditional 'for loops' for IE 11
        for(let mutation of mutationsList) {
            chrome.runtime.sendMessage({
                content: mutation.target.textContent
            });
        }
    };

// Create an observer instance linked to the callback function
    const observer = new MutationObserver(callback);

// Start observing the target node for configured mutations
    observer.observe(targetNode, config);
};

content();
