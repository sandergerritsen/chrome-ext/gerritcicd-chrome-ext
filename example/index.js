'use strict';

// Example - get commit information from webserver
// setInterval(function() {
//     var xhttp = new XMLHttpRequest();
//     xhttp.onreadystatechange = function() {
//         if (this.readyState === 4 && this.status === 200) {
//             document.getElementById('contentHash').textContent = this.responseText;
//         }
//     };
//     xhttp.open("GET", "api.php", true);
//     xhttp.send();
// }, 10000);


document.getElementById('btn1').value = JSON.stringify({
    "commit": {
        "sha":"9b955f0ff23395293a3a7b298af4980783afdaa9",
        "username":"pdevries",
        "dateTime":"2020-03-21 11:28:55",
        "message":"Merged \"US 8764 PostNL verifier\" to \"master\""
    }
});
document.getElementById('btn2').value = JSON.stringify({
    "commit": {
        "sha":"dea72b00804f95293a3783afdaa9a7b298af4980",
        "username":"sbolkamp",
        "dateTime":"2020-03-21 11:38:03",
        "message":"Merged \"US 8944 App Dockerfile\" to \"master\""
    }
});
document.getElementById('btn3').value = JSON.stringify({
    "commit": {
        "sha":"2ebf5406b13d952f4980783afdaa993a3a7b298a",
        "username":"ghaasbeek",
        "dateTime":"2020-03-24 09:03:49",
        "message":"Merged \"US 8967 RabbitMQ event listener\" to \"master\""
    }
});
document.getElementsByName('btn').forEach(function(el) {
  el.onclick = function() {
      document.getElementById('contentHash').textContent = this.value;
  }
});
