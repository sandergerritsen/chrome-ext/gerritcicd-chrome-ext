<?php
declare(strict_types=1);

echo json_encode([
    'commit' => [
        'sha' => substr(sha1('a' . random_int(9, 99999) . time()), 0, 12),
        'username' => random_int(9,99) > 40 ? 'hflorens' : 'avandenbrink',
        'dateTime' => date("Y-m-d H:i:s"),
        'message' => 'Merged "US ' . random_int(1000, 9999) . ' ' . (random_int(9,99)>50 ? 'PostNL verifier' : 'RabbitMQ Default listener') . ' to "master"'
    ]
]);
