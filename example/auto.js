'use strict';

// Example - get commit information from webserver
setInterval(function() {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            document.getElementById('contentHash').textContent = this.responseText;
        }
    };
    xhttp.open("GET", "api.php", true);
    xhttp.send();
}, 10000);

